# - Try to find ProtocolBuffers
# Once done this will define
#  PROTOCOLBUFFERS_FOUND - System has ProtocolBuffers
#  ProtocolBuffers_INCLUDE_DIRS - The ProtocolBuffers include directories
#  ProtocolBuffers_LIBRARIES - The libraries needed to use ProtocolBuffers
#  ProtocolBuffers_EXECUTABLE - The executable needed to use ProtocolBuffers

find_package(PkgConfig)
pkg_check_modules(PC_PROTOCOLBUFFERS protobuf)
# message(STATUS "PKG_CONFIG_FOUND: ${PKG_CONFIG_FOUND}")
# message(STATUS "PKG_CONFIG_EXECUTABLE: ${PKG_CONFIG_EXECUTABLE}")
# message(STATUS "PKG_CONFIG_VERSION_STRING: ${PKG_CONFIG_VERSION_STRING}")
# message(STATUS "PC_PROTOCOLBUFFERS_FOUND: ${PC_PROTOCOLBUFFERS_FOUND}")
# message(STATUS "PC_ProtocolBuffers_INCLUDE_DIRS: ${PC_ProtocolBuffers_INCLUDE_DIRS}")
# message(STATUS "PC_PROTOCOLBUFFERS_LIBRARY_DIRS: ${PC_PROTOCOLBUFFERS_LIBRARY_DIRS}")

find_path(PROTOCOLBUFFERS_INCLUDE_DIR stubs/common.h
          HINTS ${PC_ProtocolBuffers_INCLUDE_DIRS}
          PATH_SUFFIXES google/protobuf )

find_library(PROTOCOLBUFFERS_LIBRARY NAMES protobuf
             HINTS ${PC_PROTOCOLBUFFERS_LIBRARY_DIRS} )

find_program(ProtocolBuffers_EXECUTABLE protoc)

set(ProtocolBuffers_LIBRARIES ${PROTOCOLBUFFERS_LIBRARY} )
set(ProtocolBuffers_INCLUDE_DIRS ${PROTOCOLBUFFERS_INCLUDE_DIR} )
# message(STATUS "PROTOCOLBUFFERS_LIBRARY: ${PROTOCOLBUFFERS_LIBRARY}")
# message(STATUS "PROTOCOLBUFFERS_INCLUDE_DIR: ${PROTOCOLBUFFERS_INCLUDE_DIR}")
# message(STATUS "ProtocolBuffers_EXECUTABLE: ${ProtocolBuffers_EXECUTABLE}")

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set PROTOCOLBUFFERS_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(ProtocolBuffers DEFAULT_MSG
                                  ProtocolBuffers_LIBRARIES ProtocolBuffers_INCLUDE_DIRS ProtocolBuffers_EXECUTABLE)

mark_as_advanced(ProtocolBuffers_INCLUDE_DIRS ProtocolBuffers_LIBRARIES ProtocolBuffers_EXECUTABLE)

# ensure that they are cached
SET(ProtocolBuffers_INCLUDE_DIRS ${ProtocolBuffers_INCLUDE_DIRS} CACHE INTERNAL "The protocol buffers include path")
SET(ProtocolBuffers_LIBRARIES ${ProtocolBuffers_LIBRARIES} CACHE INTERNAL "The libraries needed to use protocol buffers library")
SET(ProtocolBuffers_EXECUTABLE ${ProtocolBuffers_EXECUTABLE} CACHE INTERNAL "The protocol buffers compiler")

# Requires: 
# * ProtocolBuffers_EXECUTABLE
# * ProtocolBuffers_INCLUDE_DIRS
# * ProtocolBuffers_LIBRARIES
#
# as provided by cortex_appsdeps/protobuf/cmake/Modules/FindProtocolBuffers.cmake or manually

# run after catkin_destinations() and before catkin_python_setup()
# provides ${PROTOGEN_INCLUDE_DIRS} for catkin_package(INCLUDE_DIRS)
function(proto_destinations)
  set(PROTO_GEN_CPP_DIR ${CATKIN_DEVEL_PREFIX}/${CATKIN_PACKAGE_INCLUDE_DESTINATION} CACHE INTERNAL "Protocol buffer generation dir, cpp")
  set(PROTO_GEN_PYTHON_DIR ${CATKIN_DEVEL_PREFIX}/${CATKIN_PACKAGE_PYTHON_DESTINATION} CACHE INTERNAL "Protocol buffer generation dir, python")
  file(MAKE_DIRECTORY ${PROTO_GEN_CPP_DIR})
  file(MAKE_DIRECTORY ${PROTO_GEN_PYTHON_DIR})
  set(PROTOGEN_INCLUDE_DIRS ${PROTO_GEN_CPP_DIR}/../ ${PROTO_GEN_PYTHON_DIR} CACHE INTERNAL "Protocol buffer include dirs")
  message(STATUS "Proto Include Dirs: ${PROTOGEN_INCLUDE_DIRS}")

  message(STATUS "checking for __init__.py")
  if(NOT EXISTS ${PROTO_GEN_PYTHON_DIR}/__init__.py)
    message(STATUS "creating for __init__.py")
    file(WRITE ${PROTO_GEN_PYTHON_DIR}/__init__.py "")
    set_source_files_properties(${PROTO_GEN_PYTHON_DIR}/__init__.py PROPERTIES GENERATED TRUE)
    install(FILES ${PROTO_GEN_PYTHON_DIR}/__init__.py
      DESTINATION ${CATKIN_PACKAGE_PYTHON_DESTINATION}
    )
  endif(NOT EXISTS ${PROTO_GEN_PYTHON_DIR}/__init__.py)
endfunction()

# run after catkin_package()
# provides ${PROJECT_NAME}_proto for catkin_package(LIBRARIES)
function(proto_generate PROTO_DIR PROTO_FILE_NAMES)
  set(proto_files "")
  foreach(proto_file_name ${PROTO_FILE_NAMES})
    list(APPEND proto_files "${PROTO_DIR}/${proto_file_name}")
  endforeach(proto_file_name ${PROTO_FILE_NAMES})
  
  message(STATUS "Proto Source Dir: ${PROTO_DIR}")
  message(STATUS "Proto Source Files: ${proto_files}")

  set(proto_gen_cpp_files "")
  set(proto_gen_python_files "")
  foreach(proto_file ${proto_files})
    get_filename_component(proto_name ${proto_file} NAME_WE)
    list(APPEND proto_gen_cpp_files ${PROTO_GEN_CPP_DIR}/${proto_name}.pb.h ${PROTO_GEN_CPP_DIR}/${proto_name}.pb.cc)
    list(APPEND proto_gen_python_files ${PROTO_GEN_PYTHON_DIR}/${proto_name}_pb2.py)
  endforeach(proto_file ${proto_files})

  # Run protoc and generate language-specific headers.
  add_custom_command(
    OUTPUT ${proto_gen_cpp_files} ${proto_gen_python_files}
    COMMAND ${ProtocolBuffers_EXECUTABLE} --proto_path=${PROTO_DIR} --cpp_out=${PROTO_GEN_CPP_DIR} --python_out=${PROTO_GEN_PYTHON_DIR} ${proto_files}
    DEPENDS ${ProtocolBuffers_EXECUTABLE} ${proto_files}
    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
  )

  set_source_files_properties(${proto_gen_cpp_files} PROPERTIES GENERATED TRUE)
  set_source_files_properties(${proto_gen_python_files} PROPERTIES GENERATED TRUE)

  add_custom_target(${PROJECT_NAME}_generate_headers
    DEPENDS ${proto_gen_cpp_files} ${proto_gen_python_files}
  )

  # Create proto library for linking.
  include_directories(${ProtocolBuffers_INCLUDE_DIRS} ${ProtocolBuffers_INCLUDE_DIRS}/../../)
  add_library(${PROJECT_NAME}_proto ${proto_gen_cpp_files})
  target_link_libraries(${PROJECT_NAME}_proto ${ProtocolBuffers_LIBRARIES})
  add_dependencies(${PROJECT_NAME}_proto ${PROJECT_NAME}_generate_headers)

  install(TARGETS ${PROJECT_NAME}_proto
    ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
    LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
    RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
  )

  install(DIRECTORY ${PROTO_GEN_CPP_DIR}/
    DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
    FILES_MATCHING PATTERN "*.h"
  )

  install(DIRECTORY ${PROTO_GEN_PYTHON_DIR}/
    DESTINATION ${CATKIN_PACKAGE_PYTHON_DESTINATION}
    FILES_MATCHING 
      PATTERN "*.py"
      PATTERN "setup.py" EXCLUDE
      PATTERN "__init__.py" EXCLUDE
  )
endfunction()
