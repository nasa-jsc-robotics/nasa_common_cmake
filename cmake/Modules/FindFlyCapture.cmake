# - Try to find FlyCapture
# Once done this will define
#  FlyCapture_FOUND - System has FlyCapture
#  FlyCapture_INCLUDE_DIRS - The FlyCapture include directories
#  FlyCapture_LIBRARIES - The libraries needed to use FlyCapture

find_package(PkgConfig)
pkg_check_modules(PC_FLYCAPTURE libflycapture)
# message(STATUS "PKG_CONFIG_FOUND: ${PKG_CONFIG_FOUND}")
# message(STATUS "PKG_CONFIG_EXECUTABLE: ${PKG_CONFIG_EXECUTABLE}")
# message(STATUS "PKG_CONFIG_VERSION_STRING: ${PKG_CONFIG_VERSION_STRING}")
# message(STATUS "PC_FLYCAPTURE_FOUND: ${PC_FLYCAPTURE_FOUND}")
# message(STATUS "PC_FlyCapture_INCLUDE_DIRS: ${PC_FlyCapture_INCLUDE_DIRS}")
# message(STATUS "PC_FLYCAPTURE_LIBRARY_DIRS: ${PC_FLYCAPTURE_LIBRARY_DIRS}")

find_path(FLYCAPTURE_INCLUDE_DIR flycapture/FlyCapture2.h
          HINTS ${PC_FlyCapture_INCLUDE_DIRS}
          PATH_SUFFIXES include)

find_library(FLYCAPTURE_LIBRARY NAMES flycapture
             HINTS ${PC_FLYCAPTURE_LIBRARY_DIRS})

set(FlyCapture_LIBRARIES ${FLYCAPTURE_LIBRARY})
set(FlyCapture_INCLUDE_DIRS ${FLYCAPTURE_INCLUDE_DIR})
# message(STATUS "FLYCAPTURE_LIBRARY: ${FLYCAPTURE_LIBRARY}")
# message(STATUS "FLYCAPTURE_INCLUDE_DIR: ${FLYCAPTURE_INCLUDE_DIR}")

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set FLYCAPTURE_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(FlyCapture DEFAULT_MSG
                                  FlyCapture_LIBRARIES FlyCapture_INCLUDE_DIRS)

mark_as_advanced(FlyCapture_INCLUDE_DIRS FlyCapture_LIBRARIES)

# ensure that they are cached
SET(FlyCapture_INCLUDE_DIRS ${FlyCapture_INCLUDE_DIRS} CACHE INTERNAL "The libflycapture include path")
SET(FlyCapture_LIBRARIES ${FlyCapture_LIBRARIES} CACHE INTERNAL "The libraries needed to use libflycapture library")
