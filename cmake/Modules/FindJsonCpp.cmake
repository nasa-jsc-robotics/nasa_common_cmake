# - Try to find JsonCpp
# Once done this will define
#  JsonCpp_FOUND - System has JsonCpp
#  JsonCpp_INCLUDE_DIRS - The JsonCpp include directories
#  JsonCpp_LIBRARIES - The libraries needed to use JsonCpp

find_package(PkgConfig)
pkg_check_modules(PC_JSONCPP jsoncpp)
# message(STATUS "PKG_CONFIG_FOUND: ${PKG_CONFIG_FOUND}")
# message(STATUS "PKG_CONFIG_EXECUTABLE: ${PKG_CONFIG_EXECUTABLE}")
# message(STATUS "PKG_CONFIG_VERSION_STRING: ${PKG_CONFIG_VERSION_STRING}")
# message(STATUS "PC_JSONCPP_FOUND: ${PC_JSONCPP_FOUND}")
# message(STATUS "PC_JSONCPP_INCLUDE_DIRS: ${PC_JSONCPP_INCLUDE_DIRS}")
# message(STATUS "PC_JSONCPP_LIBRARY_DIRS: ${PC_JSONCPP_LIBRARY_DIRS}")

find_path(JSONCPP_INCLUDE_DIR json/json.h
          HINTS ${PC_JSONCPP_INCLUDE_DIRS}
          PATH_SUFFIXES include)

find_library(JSONCPP_LIBRARY NAMES jsoncpp
             HINTS ${PC_JSONCPP_LIBRARY_DIRS})

set(JsonCpp_LIBRARIES ${JSONCPP_LIBRARY})
set(JsonCpp_INCLUDE_DIRS ${JSONCPP_INCLUDE_DIR})
# message(STATUS "JSONCPP_LIBRARY: ${JSONCPP_LIBRARY}")
# message(STATUS "JSONCPP_INCLUDE_DIR: ${JSONCPP_INCLUDE_DIR}")

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set JSONCPP_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(JsonCpp DEFAULT_MSG
                                  JsonCpp_LIBRARIES JsonCpp_INCLUDE_DIRS)

mark_as_advanced(JsonCpp_INCLUDE_DIRS JsonCpp_LIBRARIES)

# ensure that they are cached
SET(JsonCpp_INCLUDE_DIRS ${JsonCpp_INCLUDE_DIRS} CACHE INTERNAL "The jsoncpp include path")
SET(JsonCpp_LIBRARIES ${JsonCpp_LIBRARIES} CACHE INTERNAL "The libraries needed to use jsoncpp library")
